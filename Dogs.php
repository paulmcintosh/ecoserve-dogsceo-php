<?php

//namespace BSG;

/**
 * ServicesContainer interface
 *
 * @package default
 * @author Paul McIntosh
 */
interface ServicesContainer {

	public function addService(Services $item);

}

/**
 * Service class implements ServicesContainer
 *
 * @package default
 */
class Service implements ServicesContainer {

	public $items;

	public function addService(Services $item) {
				
		return false;
	}


}

/**
 * Services Interface
 *
 * @package default
 */
interface Services {

	public function isService();
	
	public function formatResults();

}

/**
 * Serve abstract class implements Services
 *
 * @package default
 */
abstract class Serve{

	protected $service;
	
	
	public $calls = array(
		'allBreeds'=>'https://dog.ceo/api/breeds/list/all',
		'random'=>'https://dog.ceo/api/breeds/image/random',
		'byBreed'=>'https://dog.ceo/api/breeds/image/random'
	);
	
	public $url;

	public function isService() {
		
		$respose = file_get_contents($this->url);
		
		return json_decode($respose);
	}
	
	public function formatResults($service){
		
		if(isset($service->message)){
			
			$result = array();
						
			foreach ($service->message as $key => $value) {
				
				$breed = array('breed'=>$key);
				
				$breed = $this->formatSubBreed($key, $value, $breed);
				
				$result[] = $breed;
			}

			return $result;
			
		} else {
			
			return false;
		}
	}
	
	public function formatSubBreed($key,$value,$breed){
		
		if(isset($value[0])){
						
			for ($i=0; $i < count($value); $i++) { 
				
				$breed['sub_breed_' . $key] = $value[$i];
			}
						
		}
		
		return $breed;
	}

}

/**
 * StartService class
 *
 * @package default
 */
class StartService extends Serve {

	
	public function __construct($request,$opts=false){
		$this->url = $this->calls[$request];
	}
	
	/**
	 * Restful call to endpoint
	 */
	public function endPoint(){
		
		$service = $this->isService();
		
		$response = $this->formatResults($service);
		
		return $response;
	}

}

/**
 * StartService class
 *
 * @package default
 */
class StartServiceRandom extends Serve {

	
	public function __construct($request){
		
		$this->url = $this->calls[$request];
	}
	
	/**
	 * Restful call to endpoint
	 */
	public function endPoint(){
				
		return $this->isService();
	}

}

/**
 * StartService class
 *
 * @package default
 */
class StartServiceByBreed extends Serve {

	
	public function __construct($request){
		
		$this->url = 'https://dog.ceo/api/breed/'. $request .'/images';
	}
	
	/**
	 * Restful call to endpoint
	 */
	public function endPoint(){
				
		return $this->isService();
	}

}

/**
 * StartService class
 *
 * @package default
 */
class StartServicebySubBreed extends Serve {

	
	public function __construct($request){
		
		$this->url = 'https://dog.ceo/api/breed/hound/'. $request .'/images';
		
	}
	
	/**
	 * Restful call to endpoint
	 */
	public function endPoint(){
		
		return $this->isService();
	}

}

/**
 * Dogs class
 *
 * @package default
 */
class Dogs extends Service{

	public function allBreeds(){
		
		$StartService = new StartService(__FUNCTION__);

		return $StartService->endPoint();
		
		
	}
	
	public function random(){
		
		$StartService = new StartServiceRandom(__FUNCTION__);
		
		return $StartService->endPoint();
		
	}
	
	public function byBreed($breed){
		
		$StartService = new StartServiceByBreed($breed);
		
		return $StartService->endPoint();
		
	}
	
	public function bySubBreed($breed){
		
		$StartService = new StartServicebySubBreed($breed);
		
		return $StartService->endPoint();
		
	}
}
?>